-- [[ Basic Keymaps ]]
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)


-- define useful variables
-- TODO: i dont get them!
local term_opts = { silent = true }
local opts = { noremap=true, silent=true}

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--  remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- See `:help vim.keymap.set()`


-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)--it tells in normal mode change <C-w>h to <C-h> so we have better navigation between windows!
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

keymap("n", "<leader>e", ":Lex 20<cr>", opts) --netrW is the default explorer! when we do Lex we call him. 30 staand for the size and <cr> for pressing enter!(carriage  return)



-- Resize with arrows
-- Up and down have some issues TODO: try to fix that
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts) --S stands for Shift key
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Insert --
-- Press jk fast to enter i dislike it so i will not use it
--keymap("i", "jk", "<ESC>", opts)

-- Visual --
-- Stay in indent mode i LIKE IT
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts) --A is for the Alt key
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts)--allow to keep the original thing we yanked, and not put the deleted one in the register


-- Visual Block --
-- Move text up and down (same as visual up there just to take care of when we select multiple lines)
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)


-- Terminal --
-- Better terminal navigation
-- TODO: i dont understand this one
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

--Telescope Keymaps
-- See `:help telescope.builtin`
keymap("n", "<leader>ff", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
keymap("n", "<leader>sf", "<cmd>Telescope find_files <cr>", opts)
keymap("n", "<c-t>", "<cmd>Telescope live_grep<cr>", opts) --search by grep
--TODO:add keymaps
--vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
--vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
--[[ vim.keymap.set('n', '<leader>/', function() -- You can pass additional configuration to telescope to change theme, layout, etc. require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown { winblend = 10, previewer = false, }) end, { desc = '[/] Fuzzily search in current buffer]' })
--]]
--vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })
--vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
--vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
keymap("n", "<leader>sh", "<cmd>lua require'telescope.builtin'.help_tags()<cr>", opts)
keymap("n", '<leader>sw', "<cmd>lua require'telescope.builtin'.grep_string()<cr>", { desc = '[S]earch current [W]ord' })
keymap("n", "<leader>/", "<cmd>lua require'telescope.builtin'.current_buffer_fuzzy_find(require'telescope.themes'.get_dropdown { winblend = 10, previewer = false, })<cr>", opts)
keymap("n", '<leader>sd',"<cmd>lua require'telescope.builtin'.diagnostics()cr>", opts)
keymap("n", '<leader><space>',"<cmd>lua require'telescope.builtin'.buffers()<cr>", opts)
keymap("n", '<leader>?',"<cmd>lua require'telescope.builtin'.oldfiles()<cr>", opts)












-- Remap for dealing with word wrap
-- TODO: i dont get it!
--vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
--vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

