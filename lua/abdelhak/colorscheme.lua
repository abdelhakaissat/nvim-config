local colorscheme = "dracula"

 -- this code uses that protected call 
 -- basically it says do vim.cmd "colorscheme dracula" and in the status_ok 
 -- it will save whether or not there is an error , the _ is just like python
 -- we don't need it it's just there because pcall returns 2 things
 -- the .. is the lua way to use a variable i believe
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end
