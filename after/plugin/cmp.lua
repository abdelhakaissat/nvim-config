-- nothing fancy we do a protect call 
local cmp_status_ok, cmp = pcall(require, "cmp")
if not cmp_status_ok then
  return
end

-- same protect call here
local snip_status_ok, luasnip = pcall(require, "luasnip")
if not snip_status_ok then
  return
end

-- If you want insert `(` after select function or method item
local nvmim_cmp_stat , cmp_autopairs = pcall(require,"nvim-autopairs.completion.cmp")
if not nvmim_cmp_stat then
  print("something wrong in nvim-autopairs.completion.cmp")
  return
end

cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

-- To use existing VS Code style snippets from a plugin (eg. rafamadriz/friendly-snippets)
-- simply install the plugin and then add
require("luasnip.loaders.from_vscode").lazy_load()
--somewhere in your nvim-config. LuaSnip will then load the snippets contained in the plugin 
--on startup. You can also easily load your own custom vscode style snippets by passing the 
--path to the custom snippet-directory to the load function:




-- apparently it will just make	things work better (shift related i believe?)
local check_backspace = function()
  local col = vim.fn.col "." - 1
  return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
end


--   פּ ﯟ   some other good icons
-- TODO: fix this!
local kind_icons = {
  Text = "",
  Method = "m",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "",
  Interface = "",
  Module = "",
  Property = "",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = "",
}
-- find more here: https://www.nerdfonts.com/cheat-sheet

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body) -- snipet engine definiton or `luasnip` users.
    end,
  },
  mapping = {
    ["<C-k>"] = cmp.mapping.select_prev_item(),--scroll completion
    ["<C-j>"] = cmp.mapping.select_next_item(), --scrall completion
    ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),--scroll docs of completion
    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }), --scroll_docs of completion
    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }), -- 
    ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ["<C-e>"] = cmp.mapping {--get rid of the completion
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    },
    -- Accept currently selected item. If none selected, `select` first item.
    -- Set `select` to `false` to only confirm explicitly selected items.
    ["<CR>"] = cmp.mapping.confirm { select = true },

    -- here where we define what tab does according to what's going on rn
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expandable() then
        luasnip.expand()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif check_backspace() then
        fallback()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  },

  -- here we define how the completion will display (i think)
  -- the symbols are called kind abbr is for abbreviation and metnu is 
  -- for the menu when it will display maybe the function docs.
  -- we can change the order if we want .. but i don't!
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      -- Kind icons
      vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind

      -- this is how we name the different suggestions we have!
      vim_item.menu = ({
        nvim_lsp = "[LSP]",
	nvim_lua = "nvim_lua",
        luasnip = "[Snippet]",
        buffer = "[Buffer]",
        path = "[Path]",
      })[entry.source.name]
      return vim_item
    end,
  },

  -- this is where we order
  sources = {
    { name = "nvim_lsp" },
    { name = "nvim_lua" },
    { name = "luasnip" },
    { name = "buffer" },
    { name = "path" },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
  window = {
    documentation = cmp.config.window.bordered(),
  },
  experimental = {
    ghost_text = true,
    native_menu = false,
  },
}
