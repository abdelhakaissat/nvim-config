local fn = vim.fn

-- lazyloading is when you start a package only if a certain condition is
-- true, like you open a specific file or something, check the packer
-- documentation for more details on that
-- there is also the event thing check :help autocmd 



--  the data directory is in .local/share/nvim if you wanna check them out

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then --if it's not installed it will clone the repo and install it
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer") --its a protected call!
if not status_ok then -- if packer is not installed we quit!
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(function(use)
  -- My plugins here
  use "wbthomason/packer.nvim" -- Have packer manage itself


  use "nvim-lua/popup.nvim" -- Useful for telescope medioa files!
  use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins

  use "kdheepak/lazygit.nvim"

  -- Completion plugins 
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use "hrsh7th/cmp-nvim-lsp" -- completions for lsp
  use "hrsh7th/cmp-nvim-lua" -- completion for neovim lua Api

  -- snippets
  use "L3MON4D3/LuaSnip" --snippet engine/ there are other snippets engines!
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use		 


  use 'lukas-reineke/indent-blankline.nvim' -- Add indentation guides even on blank lines

  -- LSP configuration & plugins
  use {
      "neovim/nvim-lspconfig", -- enable LSP
      requires = {
      -- Automatically Install LSPs to stdpath for neovim
        use "williamboman/mason.nvim", -- simple to use language server installer
        use "williamboman/mason-lspconfig.nvim" -- simple to use language server installer
      },
  }
  use 'jose-elias-alvarez/null-ls.nvim' -- LSP diagnostics and code actions

  -- statusline
  use {
    'nvim-lualine/lualine.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }

  -- Telescope
   use "nvim-telescope/telescope.nvim"
   use "nvim-telescope/telescope-media-files.nvim" --to display images!
    -- Fuzzy Finder Algorithm which requires local dependencies to be built. Only load if `make` is available
   use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable 'make' == 1 }
   use "nvim-telescope/telescope-file-browser.nvim"

-- terminal toggle 
  use {"akinsho/toggleterm.nvim", tag = '*', config = function()
      require("toggleterm").setup()
   end
  }

 -- Treesitter!
 use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }
  use "HiPhish/nvim-ts-rainbow2"

  -- AUTOPAIR
  use "windwp/nvim-autopairs"


  -- Colorschemes
  use "lunarvim/colorschemes" -- A bunch of colorschemes you can try out
  use "lunarvim/darkplus.nvim"
  use 'folke/tokyonight.nvim'
  use {'dracula/vim', as = 'dracula'}





  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)

-- little not for me about the definiton of Bootstraping from wikipedia:
-- 	In general, bootstrapping usually refers to a self-starting 
-- 	process that is supposed to continue or grow without external input.
