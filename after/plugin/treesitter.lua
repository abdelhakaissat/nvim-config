-- TODO you can read more on the official documentation!
local configs = require("nvim-treesitter.configs")
configs.setup {
  -- A list of parser names, or "all" (that should always be isntalled!
  ensure_installed = "all",
   -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  ignore_install = { "" }, -- List of parsers to ignore installing
  highlight = { enable = true, -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    -- Abdelhak: the neovim from scrach guy just put it like this, but the comment below are mine
    additional_vim_regex_highlighting = true,

  },
  indent = { enable = true, disable = { "yaml" } },--this is to ignore yaml in the indent thingy


  -- HiPhish/nvim-ts-rainbow2 usage of this module of treesitter
  rainbow = {
    enable = true,
    -- list of languages you want to disable the plugin for
    disable = { "jsx", "cpp" },
    -- Which query to use for finding delimiters
    query = 'rainbow-parens',
    -- Highlight the entire buffer all at once
    strategy = require 'ts-rainbow.strategy.global',
    -- Do not enable for files with more than n lines
    max_file_lines = 3000
  }

}
