local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  print("this didnt work")
  return
end

require "abdelhak.lsp.mason"
require("abdelhak.lsp.handlers").setup()
require "abdelhak.lsp.null-ls"
